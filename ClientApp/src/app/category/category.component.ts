import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-category-component',
  templateUrl: './category.component.html'
})
export class CategoryComponent {
  public categories: Category[];
  baseUrl = null;
  showCreateForm = false;
  newProperty = null;
  newCategory = {
    id:-1,
    name:null,
    properties:[]
  };

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string,private router:Router) {
    this.baseUrl = baseUrl;
    this.getAll();
  }

  getAll(){
    this.http.get<Category[]>(this.baseUrl + 'api/Category/GetAll').subscribe(result => {
      this.categories = result;
    }, error => console.error(error));
  }

  goToCategory(id){
    this.router.navigateByUrl('/categories/edit/'+id);
  }

  removeCategory(category){
    if (confirm('Вы хотите удалить категорию - '+category.name+' ?')) {
      this.http.delete(this.baseUrl + 'api/Category/Delete/'+category.id).subscribe(result => {
        console.log(result);
        this.getAll();
      }, error => console.error(error));

    } 
  }
}

interface Category {
  id: number;
  name: string;
  properties: string[];
}
