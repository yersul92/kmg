import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-category-form-component',
  templateUrl: './form.component.html'
})
export class CategoryFormComponent {
  baseUrl = null;
  newProperty = null;
  id=-1;
  category = {
    id:-1,
    name:null,
    properties:[]
  };

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string,private route:ActivatedRoute,private router:Router) {
    this.baseUrl = baseUrl;
    this.id = this.route.snapshot.params['id'];
    if(this.id>0){
      this.getCategoryById();
    }
  }

  goToCategories(){
    this.router.navigateByUrl('/categories');
  }

  getCategoryById(){
    this.http.get<Category>(this.baseUrl + 'api/Category/GetById/'+this.id).subscribe(result => {
      this.category = result;
    }, error => console.error(error));
  }

  addProperty(){
    this.category.properties.push(this.newProperty);
    this.newProperty = null;
  }
  
  removeProperty(ind){
    this.category.properties.splice(ind,1);
  }

  onSubmit(){
    if(this.id==-1){
      this.http.post<Category>(this.baseUrl + 'api/Category/Insert',this.category).subscribe(result => {
         alert("Новая категория успешно создана");
         this.router.navigateByUrl('/categories/edit/'+result.id);
      }, error => console.error(error));
    }else{
      this.http.put<Category>(this.baseUrl + 'api/Category/Update/'+this.id,this.category).subscribe(result => {
        alert("Изменения успешно внесены");
      }, error => console.error(error));
    }
    
  }

  cancel(){
    this.goToCategories();
  }

}

interface Category {
  id: number;
  name: string;
  properties: string[];
}
