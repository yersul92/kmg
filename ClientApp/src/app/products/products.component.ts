import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-products-component',
  templateUrl: './products.component.html'
})
export class ProductsComponent {
  public products: Product[];
  baseUrl = null;
  public categories: Category[];
  id:-1;
  selectedCategory = null;
  searchProduct = {
    name:'',
    description:'',
    cost:'',
    category:{
      id:-1,
      name:null,
      properties:{}
    }
  };


  
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string,private router:Router) {
    this.baseUrl = baseUrl;
    this.getAll();
    this.getAllCategories()
  }
  objectKeys(obj) {
    return Object.keys(obj);
  }
  getAll(){
    this.http.get<Product[]>(this.baseUrl + 'api/Product/GetAll').subscribe(result => {
      this.products = result;
    }, error => console.error(error));
  }
  getAllCategories(){
    this.http.get<Category[]>(this.baseUrl + 'api/Category/GetAll').subscribe(result => {
      this.categories = result;
    }, error => console.error(error));
  }

  goToProduct(id){
    this.router.navigateByUrl('/products/edit/'+id);
  }

  removeProduct(product){
    if (confirm('Вы хотите удалить товар - '+product.name+' ?')) {
      this.http.delete(this.baseUrl + 'api/Product/Delete/'+product.id).subscribe(result => {
        console.log(result);
        this.getAll();
      }, error => console.error(error));

    } 
  }
  search(){
    var d = this.searchProduct;
    var name = this.searchProduct.name?this.searchProduct.name:"";
    var description = this.searchProduct.description?this.searchProduct.description:"";
    var cost = this.searchProduct.cost?this.searchProduct.cost:-1;
    var cat = this.searchProduct.category.id?this.searchProduct.category.id:-1;
    
    var catPropsArr = [];
    for (var k in this.searchProduct.category.properties) {
       if(this.searchProduct.category.properties[k]){
          catPropsArr.push(k+"="+this.searchProduct.category.properties[k]);
       }
    }
    var catProps = catPropsArr.join('&');
    var url = this.baseUrl + 'api/Product/Search?name='+name+'&description='+description+'&cost='+cost+'&category_id='+cat+'&category_props='+catProps;
    this.http.get<Product[]>(url).subscribe(result => {
      this.products = result;
    }, error => console.error(error));
  }
  selectChanged(){
    if(this.searchProduct.category.id==-1){
      this.selectedCategory = null;
      return;
    }
    for (let i = 0; i < this.categories.length; i++) {
      if(this.categories[i].id==this.searchProduct.category.id){
        this.selectedCategory = this.categories[i];
        for (let j = 0; j < this.selectedCategory.properties.length; j++) {
          this.searchProduct.category.properties[this.selectedCategory.properties[j]] = null;
        }
        var keys_ =  this.objectKeys(this.searchProduct.category.properties);
        for (let i = 0; i < keys_.length; i++) {
          if(this.selectedCategory.properties.indexOf(keys_[i])>-1){

          }else{
            delete this.searchProduct.category.properties[keys_[i]];
          }      
        }
        break;
      }      
    }
  }

}

interface Product {
  id: number;
  name: string;
  description: string;
  cost: string;
  properties: string[];
}

interface Category_ {
  id: number;
  name: string;
  properties: {};
}

interface Category {
  id: number;
  name: string;
  properties: string[];
}