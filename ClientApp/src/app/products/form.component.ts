import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-product-form-component',
  templateUrl: './form.component.html'
})
export class ProductFormComponent {
  public categories: Category[];
  baseUrl = null;
  selectedCategory = null;
  id=-1;
  product = {
    id:-1,
    name:'',
    description:'',
    cost:'',
    category:{
      id:-1,
      name:null,
      properties:{}
    }
  };

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string,private route:ActivatedRoute,private router:Router) {
    this.baseUrl = baseUrl;
    this.id = this.route.snapshot.params['id'];
    this.getAllCategories();
    
  }
  getAllCategories(){
    this.http.get<Category[]>(this.baseUrl + 'api/Category/GetAll').subscribe(result => {
      this.categories = result;
      if(this.id>0){
        this.getProductById();
      }
    }, error => console.error(error));
  }

  objectKeys(obj) {
    return Object.keys(obj);
  }

  goToProducts(){
    this.router.navigateByUrl('/products');
  }

  getProductById(){
    this.http.get<Product>(this.baseUrl + 'api/Product/GetById/'+this.id).subscribe(result => {
       this.product = result;
       for (let i = 0; i < this.categories.length; i++) {
          if(this.categories[i].id==this.product.category.id){
            this.selectedCategory = this.categories[i];
            break;
          }     
       }
       
    }, error => console.error(error));
  }

  selectChanged(){
    for (let i = 0; i < this.categories.length; i++) {
      if(this.categories[i].id==this.product.category.id){
        this.selectedCategory = this.categories[i];
        for (let j = 0; j < this.selectedCategory.properties.length; j++) {
          this.product.category.properties[this.selectedCategory.properties[j]] = null;
        }
        break;
      }     
   }
  }

  onSubmit(){
    var keys_ =  this.objectKeys(this.product.category.properties);
    for (let i = 0; i < keys_.length; i++) {
       if(this.selectedCategory.properties.indexOf(keys_[i])>-1){

       }else{
         delete this.product.category.properties[keys_[i]];
       }      
    }
    this.product.category.name =this.selectedCategory.name; 
    if(this.id==-1){
      this.http.post<Product>(this.baseUrl + 'api/Product/Insert',this.product).subscribe(result => {
         alert("Новый товар успешно создан");
         this.router.navigateByUrl('/products/edit/'+result.id);
      }, error => console.error(error));
    }else{
      this.http.put<Product>(this.baseUrl + 'api/Product/Update/'+this.id,this.product).subscribe(result => {
        alert("Изменения успешно внесены");
      }, error => console.error(error));
    }
    
  }

  cancel(){
    this.goToProducts();
  }

}

interface Product {
  id: number;
  name: string;
  description: string;
  cost: string;
  category: Category_;
}

interface Category_ {
  id: number;
  name: string;
  properties: {};
}

interface Category {
  id: number;
  name: string;
  properties: string[];
}

