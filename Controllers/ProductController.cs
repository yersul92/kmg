using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace kmg.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    { 
        private static List<Product> Products = new List<Product>
        {
           new Product(){ Id = 1,Name = "Футболка", Description="Красивая футболка",Cost="45 тг", Category = new Category_(){Id=1,Name="Одежда",Properties = new Dictionary<string, string>{{"Размер","XXL"},{"Цвет","Blue"}}}},
           new Product(){ Id = 2,Name = "Обувь", Description="Красивые туфли", Cost="25 тг", Category = new Category_(){Id=2,Name="Обувь",Properties = new Dictionary<string, string>(){{"Материал","Кожа"},{"Цвет","Blue"}}}}           
        };

        [HttpGet("[action]")]
        public IEnumerable<Product> GetAll()
        {
            return Products;
        }

        [HttpGet("Search")]
        public IEnumerable<Product> Search( [FromQuery]string name, [FromQuery]string description, [FromQuery]int cost, [FromQuery]int category_id,[FromQuery]string category_props)
        {
            var res = Products;
            if(!string.IsNullOrEmpty(name)){
                res = Products.Where(x=>x.Name.ToLower().Contains(name.ToLower())).ToList();
            }
            if(!string.IsNullOrEmpty(description)){
                res = Products.Where(x=>x.Description.ToLower().Contains(description.ToLower())).ToList();
            }
            if(cost>-1){
                res = Products.Where(x=>x.Cost==cost.ToString()).ToList();
            }
            if(category_id>-1){
                res = Products.Where(x=>x.Category.Id==category_id).ToList();
                if(!string.IsNullOrEmpty(category_props)){
                    var props = category_props.Split('&');
                    foreach (string prop_ in props)
                    {
                        var keyValuePair = prop_.Split('=');
                        res = Products.Where(x=>x.Category.Id==category_id&&x.Category.Properties[keyValuePair[0]]==keyValuePair[1]).ToList();
                    }
                    
                }
            }
            
            return res;
        }

        [HttpGet("GetById/{id:int}")]
        public Product GetById(int id)
        {
            return Products.FirstOrDefault(item_=>item_.Id==id);
        }

        [HttpPost("[action]")]
        public Product Insert([FromBody]Product prod)
        {
            var lastProduct = Products.LastOrDefault();
            prod.Id = lastProduct!=null?lastProduct.Id+1:1;
            Products.Add(prod);
            return prod;
        }

        [HttpPut("Update/{id:int}")]
        public Product Update(int id,[FromBody]Product prod)
        {
            var item__ = Products.FirstOrDefault(item_ => item_.Id == id);
            if(item__!=null){
                item__.Name = prod.Name;
                item__.Description = prod.Description;
                item__.Cost = prod.Cost;
                item__.Category = prod.Category;
            }
            return prod;
        }

        [HttpDelete("Delete/{id:int}")]
        public int Delete(int id)
        {
            Products.RemoveAll((x) => x.Id == id);
            return id;
        }


        public class Product
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Cost { get; set; }
            public Category_ Category {get; set;}
        }

        public class Category_{
            public int Id { get; set; }
            public string Name { get; set; }
            public Dictionary<string,string> Properties{ get; set; }
        }
    }
}
