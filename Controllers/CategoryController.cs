using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace kmg.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private static List<Category> Categories = new List<Category>
        {
           new Category(){ Id = 1,Name = "Одежда", Properties = new List<string>{"Размер","Цвет"}},
           new Category(){ Id = 2,Name = "Обувь", Properties = new List<string>{"Цвет","Материал"}}           
        };

        [HttpGet("[action]")]
        public IEnumerable<Category> GetAll()
        {
            return Categories;
        }

        [HttpGet("GetById/{id:int}")]
        public Category GetById(int id)
        {
            return Categories.FirstOrDefault(item_=>item_.Id==id);
        }

        [HttpPost("[action]")]
        public Category Insert([FromBody]Category cat)
        {
            var lastCat = Categories.LastOrDefault();
            cat.Id = lastCat!=null?lastCat.Id+1:1;
            Categories.Add(cat);
            return cat;
        }

        [HttpPut("Update/{id:int}")]
        public Category Update(int id,[FromBody]Category cat)
        {
            var item__ = Categories.FirstOrDefault(item_ => item_.Id == id);
            if(item__!=null){
                item__.Name = cat.Name;
                item__.Properties = cat.Properties;
            }
            return cat;
        }

        [HttpDelete("Delete/{id:int}")]
        public int Delete(int id)
        {
            Categories.RemoveAll((x) => x.Id == id);
            return id;
        }


        public class Category
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public List<string> Properties {get; set;}
        }
    }
}
